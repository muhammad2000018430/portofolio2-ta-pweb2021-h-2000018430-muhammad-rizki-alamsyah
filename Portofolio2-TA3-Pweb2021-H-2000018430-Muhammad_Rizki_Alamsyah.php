<!DOCTYPE html>
<html>
<head>
    <title>Service</title>
    <link rel="stylesheet" href="Portofolio2-TA1-Pweb2021-H-2000018430-Muhammad Rizki Alamsyah.css">
    <script lang="javascript">
        function validasi(){
        var nama= document.input.inama.value;
        document.output.onama.value=nama;
        if (nama==null || nama==""){
            alert("Nama Tidak Boleh Kosong");
            return false;
        }
        
        var telp= document.input.itelp.value;
        document.output.otelp.value=telp;
        if (telp==null || telp==""){
            alert("Nomor Telepon Tidak Boleh Kosong");
            return false;
        }
        var Email= document.input.iEmail.value;
        document.output.oEmail.value=Email;
        if (Email==null || Email==""){
            alert("Email Tidak Boleh Kosong");
            return false;
        }

        var buka= document.input.ibuka.value;
        document.output.obuka.value=buka;
        if (buka==null || buka==""){
            alert("Pilihan Tempat Anda Membuka Website ini Tidak Boleh Kosong");
            return false;
        }
        var kendala= document.input.ikendala.value;
        document.output.okendala.value=kendala;
        if (kendala==null || kendala==""){
            alert("Kendala yang Anda Hadapi Tidak Boleh Kosong");
            return false;
        }
        }
        </script>
</head>
<body>
    
    <div class="container">
    <div class="header">
    <h1 class="judul">PROJECT - RIZKI A</h1>

    <hr class="line">
    <nav>
    <ul>
        <li><a href="Portofolio2-TA1-Pweb2021-H-2000018430-Muhammad Rizki Alamsyah.php">Registrasi</a></li>
        <li><a href="Portofolio2-TA2-Pweb2021-H-2000018430-Muhammad Rizki Alamsyah.php">Dosen & Staf</a></li>
        <li><a href="Portofolio2-TA3-Pweb2021-H-2000018430-Muhammad Rizki Alamsyah.php">Bantuan</a></li>
    </ul>
    </nav>
    </div>

    <div class="hero"></div>
    <div class="content cf">
    <div class="main">
    
    <h4><?php
    function hitungUmur($thn_lahir, $thn_sekarang){
        $umur=$thn_sekarang - $thn_lahir;
        return $umur;
    }

    function Perkenalan($nama, $salam="Selamat Datang di Customer Service !! <br>"){
        echo $salam;
        echo "Perkenalkan nama saya ".$nama." saya berusia ".hitungUmur(2002, 2020) ." tahun. <br>";
        echo "Senang Bisa Membantu Anda !! <br>";
    }
    perkenalan("Rizki Alamsyah");
    ?></h4>

            <h3>Silahkan isi From Kendala Berikut</h3>
            </b>
            <form name="input">
            <table border="0">
            <tr>
            <td>Nama</td><td> : </td>
            <td> <input type="text" name="inama" size="35"/> </td>
            </tr>

            <tr>
                <td>Nomor Telepon</td><td> : </td>
                <td> <input type="text" name="itelp" size="35"> </td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td><input type="text" name=iEmail size=35></td> 
           </tr>
            
            <tr>
                <td>Anda membuka website ini di</td><td> : </td>
                <td>
                <select name="ibuka">
                <option disabled selected value="">-- Pilih --</option>
                <option value="HP">HP</option>
                <option value="KOMPUTER">KOMPUTER</option>
                <option value="LAPTOP">LAPTOP</option>
                <option value="TABLET">TABLET</option>
                </select></td>
            </tr>
            <tr>
                <td>Kendala yang Dihadapi</td><td> : </td>
                <td><textarea name="ikendala" cols="36" rows="5" placeholder="Tuliskan Jenis Kendala Anda..."></textarea></td>
            </tr>
            <tr>
                <td colspan="3" align="center"><br>
                <input type="button" value="Simpan" onClick="validasi();">
                <input type="reset" value="Batal"/></td>
            </tr>
            </table>
            </form>
</table>

<b>
        <h3>Hasil Data Kendala Anda</h3>
        </b>
        <form name="output" method="POST" onsubmit="validasi()">
        <table border="0">
        <tr>
            <td>Nama </td><td> : </td>
            <td><input type="text" name="onama" size="35"/> </td>
        </tr>
        
        <tr>
            <td>Nomor Telepon</td><td> : </td>
            <td><input type="text" name="otelp" size="35"/> </td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
            <td><input type="text" name=oEmail size=35></td> 
        </tr>
        <tr>
            <td>Agama </td><td> : </td>
            <td><input type="text" name="obuka" size="35"/> </td>
        </tr>
        <tr>
            <td>Kendala yang Dihadapi </td><td> : </td>
            <td><textarea name="okendala" cols="36" rows="5"></textarea></td>
        </tr>
    </table>
    </form>
    </div>


    <div class="sidebar">
        <h3>Tentang UAD</h3>
        <img src="Logo UAD.png" alt="Logohd">
        <p>Universitas Ahmad Dahlan telah memperoleh Akreditasi Institusi
            “A” dari Badan Akreditasi Nasional Perguruan Tinggi tahun 2017. UAD
            senantiasa meningkatkan berbagai pelayanan agar menjadi tempat belajar
            yang nyaman bagi para mahasiswa, berbagai kerjasama pun terus
            ditingkatkan, mulai dari kerjasama dengan sesama lembaga pendidikan,
            lembaga riset, pemerintah, lembaga non-pemerintah dan industri, baik
            nasional maupun internasional. UAD memiliki motto “ Moral and
            Intellectual Integrity ”.</p>
        <a href="https://www.instagram.com/klik_uad/?hl=id" >Instagram</a>
        <a href="https://www.facebook.com/UADYogyakarta/">Facebook</a>
        <a href="https://twitter.com/klik_uad? ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor">Twiter</a>
        <a href="https://www.youtube.com/channel/UCEw9Tv_E3nMMIUrXIYIPsKA">Youtube</a>
    </div>
    </div>


    <div class="footer">
        <p class="Copy">Copyright &copy; Rizki Alamsyah | 2000018430</p>
    </div>
    </div>
    </div>

</body>
</html>