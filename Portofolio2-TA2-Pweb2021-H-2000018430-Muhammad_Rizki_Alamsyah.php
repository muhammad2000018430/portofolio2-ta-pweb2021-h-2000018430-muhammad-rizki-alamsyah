<!DOCTYPE html>
<html>
<head>
    <title>Dosen & Staf</title>
    <link rel="stylesheet" href="Portofolio2-TA2-Pweb2021-H-2000018430-Muhammad Rizki Alamsyah.css">
</head>
<body>
    
    <div class="container">
    <div class="header">
    <h1 class="judul">PROJECT - RIZKI A</h1>

    <hr class="line">
    <nav>
    <ul>
        <li><a href="Portofolio2-TA1-Pweb2021-H-2000018430-Muhammad Rizki Alamsyah.php">Registrasi</a></li>
        <li><a href="Portofolio2-TA2-Pweb2021-H-2000018430-Muhammad Rizki Alamsyah.php">Dosen & Staf</a></li>
        <li><a href="Portofolio2-TA3-Pweb2021-H-2000018430-Muhammad Rizki Alamsyah.php">Bantuan</a></li>
    </ul>
    </nav>
    </div>

    <div class="hero"></div>
    <div class="content cf">
    <div class="main">
    
    <?php 
	$customers = [
		[
            "No" => "1",
			"Nama" => "Adhi Prahara, S.Si., M.Cs.",
			"JK" => "L",
			"Jabatan" => "Asisten Ahli / III.A",
			"Keahlian" => "Softcomputing and Multimedia",
            "NIDN" => "0524118801",
		] ,
		[
            "No" => "2",
			"Nama" => "Anna Hendri Soleliza Jones, S. Kom, M.Cs.",
			"JK" => "P",
            "Jabatan" => "Asisten Ahli",
			"Keahlian" => "Sistem Cerdas",
            "NIDN" => "0522018302",
		],
        [
            "No" => "3",
			"Nama" => "Ahmad Azhari, S.Kom., M.Eng.",
			"JK" => "L",
            "Jabatan" => "Lektor",
			"Keahlian" => "Sistem Cerdas",
            "NIDN" => "0505118901",
		],
        [
            "No" => "4",
			"Nama" => "Bambang Robiin",
			"JK" => "L",
            "Jabatan" => "Lektor",
			"Keahlian" => "Multimedia, mobile computing",
            "NIDN" => "0020077901",
		],
        [
            "No" => "5",
			"Nama" => "Dwi Normawati, S.T., M.Eng",
			"JK" => "P",
            "Jabatan" => "Asisten Ahli",
			"Keahlian" => "Data Mining",
            "NIDN" => "0504088601",
		],
        [
            "No" => "6",
			"Nama" => "Faisal Fajri Rahani S.Si., M.Cs.",
			"JK" => "L",
            "Jabatan" => "Tenaga Pengajar",
			"Keahlian" => "Sistem cerdas, Sistem Kendali, Pengolahan Citra",
            "NIDN" => "0506079301",
		],
        [
            "No" => "7",
			"Nama" => "Fiftin Noviyanto, S.T., M. Cs",
			"JK" => "L",
            "Jabatan" => "Lektor / III.A",
			"Keahlian" => "Web Programming, Multimedia",
            "NIDN" => "0015118001",
		],
        [
            "No" => "8",
			"Nama" => "Fitri Indra Indikawati, S.Kom., M.Eng.",
			"JK" => "P",
            "Jabatan" => "Asisten Ahli",
			"Keahlian" => "Big Data Analytics, IoT Data Management",
            "NIDN" => "0515058802",
		],
        [
            "No" => "9",
			"Nama" => "Herman Yuliansyah, S.T., M. Eng",
			"JK" => "L",
            "Jabatan" => "Lektor/III.C",
			"Keahlian" => "Basis Data, Pemrograman Web, Jaringan Komputer",
            "NIDN" => "0512078304",
		],
        [
            "No" => "10",
			"Nama" => "Ika Arfiani, S.T., M.Cs.",
			"JK" => "P",
            "Jabatan" => "Asisten Ahli",
			"Keahlian" => "Rekayasa Perangkat Lunak dan Data",
            "NIDN" => "0520098702",
		],
	];

?>
	<table>
        <h3>Daftar Dosen Program Study Teknik Informatika UAD</h3>
		<tr>
            <th>No.</th>
			<th>Nama</th>
			<th>Jenis Kelamin</th>
			<th>Jabatan Fungsional</th>
			<th>Bidang Keahlian</th>
            <th>NIDN</th>

		</tr>
			<?php foreach ($customers as $customer) { ?>
				<tr>
                    <td><?php echo $customer["No"]; ?></td>
					<td><?php echo $customer["Nama"]; ?></td>
					<td><?php echo $customer["JK"]; ?></td>
                    <td><?php echo $customer["Jabatan"]; ?></td>
					<td><?php echo $customer["Keahlian"]; ?></td>
                    <td><?php echo $customer["NIDN"]; ?></td>
				</tr>
			<?php } ?>
	</table>
            </div>


    <div class="sidebar">
        <h3>Tentang UAD</h3>
        <img src="Logo UAD.png" alt="Logohd">
        <p>Universitas Ahmad Dahlan telah memperoleh Akreditasi Institusi
            “A” dari Badan Akreditasi Nasional Perguruan Tinggi tahun 2017. UAD
            senantiasa meningkatkan berbagai pelayanan agar menjadi tempat belajar
            yang nyaman bagi para mahasiswa, berbagai kerjasama pun terus
            ditingkatkan, mulai dari kerjasama dengan sesama lembaga pendidikan,
            lembaga riset, pemerintah, lembaga non-pemerintah dan industri, baik
            nasional maupun internasional. UAD memiliki motto “ Moral and
            Intellectual Integrity ”.</p>
        <a href="https://www.instagram.com/klik_uad/?hl=id" >Instagram</a>
        <a href="https://www.facebook.com/UADYogyakarta/">Facebook</a>
        <a href="https://twitter.com/klik_uad? ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor">Twiter</a>
        <a href="https://www.youtube.com/channel/UCEw9Tv_E3nMMIUrXIYIPsKA">Youtube</a>
    </div>
    </div>


    <div class="footer">
        <p class="Copy">Copyright &copy; Rizki Alamsyah | 2000018430</p>
    </div>
    </div>
    </div>

</body>
</html>