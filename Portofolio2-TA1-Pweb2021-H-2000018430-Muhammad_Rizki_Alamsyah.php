<!DOCTYPE html>
<html>
<head>
    <title>Form Pendaftaran</title>
    <link rel="stylesheet" href="Portofolio2-TA1-Pweb2021-H-2000018430-Muhammad Rizki Alamsyah.css">
    <script lang="javascript">
        function validasi(){
        var nama= document.input.inama.value;
        document.output.onama.value=nama;
        if (nama==null || nama==""){
            alert("Nama Tidak Boleh Kosong");
            return false;
        }
        var tempat= document.input.itempat.value;
        document.output.otempat.value=tempat;
        if (tempat==null || tempat==""){
            alert("TTL Tidak Boleh Kosong");
            return false;
        }
        var tgl= document.input.itgl.value;
        document.output.otgl.value=tgl;
        if (tgl==null || tgl==""){
            alert("TTL Tidak Boleh Kosong");
            return false;
        }
        
        var alamat= document.input.ialamat.value;
        document.output.oalamat.value=alamat;
        if (alamat==null || alamat==""){
            alert("Alamat Tidak Boleh Kosong");
            return false;
        }
        var telp= document.input.itelp.value;
        document.output.otelp.value=telp;
        if (telp==null || telp==""){
            alert("Nomor Telepon Tidak Boleh Kosong");
            return false;
        }
        var Email= document.input.iEmail.value;
        document.output.oEmail.value=Email;
        if (Email==null || Email==""){
            alert("Email Tidak Boleh Kosong");
            return false;
        }

        if(document.input.ijekal[0].checked==true){
        document.output.ojekal.value="Laki - Laki";
        }
        else{
        document.output.ojekal.value="Perempuan";
        }
        
        var agama= document.input.iagama.value;
        document.output.oagama.value=agama;
        if (agama==null || agama==""){
            alert("Agama Tidak Boleh Kosong");
            return false;
        }
        var nilai= document.input.inilai.value;
        document.output.onilai.value=nilai;
        if (nilai==null || nilai==""){
            alert("Nilai Rata-Rata UTBK Tidak Boleh Kosong");
            return false;
        }
        var prestasi= document.input.iprestasi.value;
        document.output.oprestasi.value=prestasi;
        if (prestasi==null || prestasi==""){
            alert("Prestasi Yang Pernah Diraih Tidak Boleh Kosong");
            return false;
        }
        }
        </script>
</head>
<body>
    
    <div class="container">
    <div class="header">
    <h1 class="judul">PROJECT - RIZKI A</h1>

    <hr class="line">
    <nav>
    <ul>
        <li><a href="Portofolio2-TA1-Pweb2021-H-2000018430-Muhammad Rizki Alamsyah.php">Registrasi</a></li>
        <li><a href="Portofolio2-TA2-Pweb2021-H-2000018430-Muhammad Rizki Alamsyah.php">Dosen & Staf</a></li>
        <li><a href="Portofolio2-TA3-Pweb2021-H-2000018430-Muhammad Rizki Alamsyah.php">Bantuan</a></li>
    </ul>
    </nav>
    </div>

    <div class="hero"></div>
    <div class="content cf">
    <div class="main">
    
    <table>
        <center>
            <h2><b><u>Form Pendaftaran Mahasiswa TI UAD</u></b></h2>
        </center>
        <b>
            <h3>Masukkan Data Anda</h3>
            </b>
            <form name="input">
            <table border="0">
            <tr>
            <td>Nama</td><td> : </td>
            <td> <input type="text" name="inama" size="35"/> </td>
            </tr>
            <tr>
                <td>Tempat, Tanggal Lahir</td>
                <td>:</td>
                <td> <input type="text" name="itempat" size="14">
                    <input type="date" name=itgl></td> 
            </tr>
            <tr>
                <td>Alamat</td><td> : </td>
                <td> <input type="text" name="ialamat" size="35" rows="5"> </td>
            </tr>
            <tr>
                <td>Nomor Telepon</td><td> : </td>
                <td> <input type="text" name="itelp" size="35"> </td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td><input type="text" name=iEmail size=35></td> 
           </tr>
            <tr>
                <td>Jenis Kelamin</td><td> : </td>
                <td>
                <input type="radio" name="ijekal" value=Laki-Laki checked>Laki-Laki
                <input type="radio" name="ijekal" value=Perempuan checked>Perempuan</td>
            </tr>
            <tr>
                <td>Agama</td><td> : </td>
                <td>
                <select name="iagama">
                <option disabled selected value="pilih">-- Pilih --</option>
                <option value="Islam">Islam</option>
                <option value="Kristen">Kristen</option>
                <option value="Katolik">Katolik</option>
                <option value="Hindu">Hindu</option>
                <option value="Budha">Budha</option>
                </select></td>
            </tr>
            <tr>
                <td>Nilai Rata-Rata UTBK Anda</td>
                <td>:</td>
                <td><input type="number" name=inilai></td> 
           </tr>
            <tr>
                <td>Prestasi yang pernah diraih</td><td> : </td>
                <td><textarea name="iprestasi" cols="36" rows="5" placeholder="Silahkan Masukkan Jika Punya..."></textarea></td>
            </tr>
            <tr>
                <td colspan="3" align="center"><br>
                <input type="button" value="Simpan" onClick="validasi();">
                <input type="reset" value="Batal"/></td>
            </tr>
            </table>
            </form>

        <b>
        <h3>Hasil Data Anda</h3>
        </b>
        <form name="output" method="POST" onsubmit="validasi()">
        <table border="0">
        <tr>
            <td>Nama </td><td> : </td>
            <td><input type="text" name="onama" size="35"/> </td>
        </tr>
        <tr>
            <td>Tempat, Tanggal Lahir</td>
            <td>:</td>
            <td> <input type="text" name="otempat" size="17"/>
                    <input type="text" name=otgl size="12"></td>  
        </tr>
        <tr>
            <td>Alamat </td><td> : </td>
            <td><input type="text" name="oalamat" size="35"/> </td>
        </tr>
        <tr>
            <td>Nomor Telepon</td><td> : </td>
            <td><input type="text" name="otelp" size="35"/> </td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
            <td><input type="text" name=oEmail size=35></td> 
        </tr>
        <tr>
            <td>Jenis Kelamin </td><td> : </td>
            <td><input type="text" name="ojekal" size="20"/> </td>
        </tr>
        <tr>
            <td>Agama </td><td> : </td>
            <td><input type="text" name="oagama" size="35"/> </td>
        </tr>
        <tr>
            <td>Nilai Rata-Rata UTBK Anda</td>
            <td>:</td>
            <td><input type="text" name=onilai size=20></td> 
        </tr>
        <tr>
            <td>Prestasi yang pernah diraih </td><td> : </td>
            <td><textarea name="oprestasi" cols="36" rows="5"></textarea></td>
        </tr>
    </table>
    </form>
    </table>
    </div>


    <div class="sidebar">
        <h3>Tentang UAD</h3>
        <img src="Logo UAD.png" alt="Logohd">
        <p>Universitas Ahmad Dahlan telah memperoleh Akreditasi Institusi
            “A” dari Badan Akreditasi Nasional Perguruan Tinggi tahun 2017. UAD
            senantiasa meningkatkan berbagai pelayanan agar menjadi tempat belajar
            yang nyaman bagi para mahasiswa, berbagai kerjasama pun terus
            ditingkatkan, mulai dari kerjasama dengan sesama lembaga pendidikan,
            lembaga riset, pemerintah, lembaga non-pemerintah dan industri, baik
            nasional maupun internasional. UAD memiliki motto “ Moral and
            Intellectual Integrity ”.</p>
        <a href="https://www.instagram.com/klik_uad/?hl=id" >Instagram</a>
        <a href="https://www.facebook.com/UADYogyakarta/">Facebook</a>
        <a href="https://twitter.com/klik_uad? ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor">Twiter</a>
        <a href="https://www.youtube.com/channel/UCEw9Tv_E3nMMIUrXIYIPsKA">Youtube</a>
        <br><br><br>
        
    </div>
    </div>

    <div class="footer">
        <p class="Copy">Copyright &copy; Rizki Alamsyah | 2000018430</p>
    </div>
    </div>
    

</body>
</html>